var ChainList = artifacts.require("./ChainList.sol");

// test suite
contract('ChainList', function (accounts) {
  var chainListInstance;
  var seller = accounts[1];
  var buyer = accounts[2];
  var articleName1 = "article 1";
  var articleDescription1 = "Description for article 1";
  var articlePrice1 = 10;
  var articleName2 = "article 2";
  var articleDescription2 = "Description for article 2";
  var articlePrice2 = 20;
  var sellerBalanceBeforeBuy, sellerBalanceAfterBuy;
  var buyerBalanceBeforeBuy, buyerBalanceAfterBuy;

  it("1. POSITIVE:: should be initialized with empty values", function () {
    return ChainList.deployed().then(function (instance) {
      chainListInstance = instance;
      return instance.getNumberOfArticles();
    }).then(function (data) {
      assert.equal(data.toNumber(), 0, "number of articles must be 0");
      return chainListInstance.getArticlesForSale();
    }).then(function (data) {
      assert.equal(data.length, 0, "there shouldn't be any articles for sale")
    })
  });

  it("2. POSITIVE:: should sell first article", function () {
    return ChainList.deployed().then(function (instance) {
      chainListInstance = instance;
      return chainListInstance.sellArticle(articleName1,
        articleDescription1,
        web3.toWei(articlePrice1, "ether"),
        { from: seller }
      );
    }).then(function (receipt) {
      assert.equal(receipt.logs.length, 1, "one event should be triggered");
      assert.equal(receipt.logs[0].event, "LogSellArticle", "event should be LogSellArticle");
      assert.equal(receipt.logs[0].args._id, 1, "id must be 1");
      assert.equal(receipt.logs[0].args._seller, seller, "seller must be " + seller);
      assert.equal(receipt.logs[0].args._name, articleName1, "article name must be " + articleName1);
      assert.equal(receipt.logs[0].args._price.toNumber(), web3.toWei(articlePrice1, "ether"), "article price must be " + web3.toWei(articlePrice1, "ether"));
      return chainListInstance.getNumberOfArticles();
    }).then(function (data) {
      assert.equal(data, 1, "number of articles should be 1");
      return chainListInstance.getArticlesForSale();
    }).then(function (data) {
      assert.equal(data.length, 1, "there must be 1 article for sale");
    })
  });


  it("3. POSITIVE:: should sell second article", function () {
    return ChainList.deployed().then(function (instance) {
      chainListInstance = instance;
      return chainListInstance.sellArticle(articleName2,
        articleDescription2,
        web3.toWei(articlePrice2, "ether"),
        { from: seller }
      );
    }).then(function (receipt) {
      assert.equal(receipt.logs.length, 1, "one event should be triggered");
      assert.equal(receipt.logs[0].event, "LogSellArticle", "event should be LogSellArticle");
      assert.equal(receipt.logs[0].args._id, 2, "id must be 2");
      assert.equal(receipt.logs[0].args._seller, seller, "seller must be " + seller);
      assert.equal(receipt.logs[0].args._name, articleName2, "article name must be " + articleName2);
      assert.equal(receipt.logs[0].args._price.toNumber(), web3.toWei(articlePrice2, "ether"), "article price must be " + web3.toWei(articlePrice2, "ether"));
      return chainListInstance.getNumberOfArticles();
    }).then(function (data) {
      assert.equal(data.toNumber(), 2, "number of articles should be 2");
      return chainListInstance.getArticlesForSale();
    }).then(function (data) {
      assert.equal(data.length, 2, "there must be 2 articles for sale");
    })
  });

  it("4. POSITIVE:: should buy an article", function () {
    return ChainList.deployed().then(function (instance) {
      chainListInstance = instance;
      sellerBalanceBeforeBuy = web3.fromWei(web3.eth.getBalance(seller), "ether").toNumber();
      buyerBalanceBeforeBuy = web3.fromWei(web3.eth.getBalance(buyer), "ether").toNumber();
      return chainListInstance.buyArticle(1, {
        from: buyer,
        value: web3.toWei(articlePrice1, "ether")
      }).then(function (receipt) {
        assert.equal(receipt.logs.length, 1, "one event should be triggered");
        assert.equal(receipt.logs[0].event, "LogBuyArticle", "event should be LogBuyArticle");
        assert.equal(receipt.logs[0].args._id, 1, "id of sold article must be 1");
        assert.equal(receipt.logs[0].args._seller, seller, "seller must be " + seller);
        assert.equal(receipt.logs[0].args._buyer, buyer, "buyer must be " + buyer);
        assert.equal(receipt.logs[0].args._name, articleName1, "article name must be " + articleName1);
        assert.equal(receipt.logs[0].args._price.toNumber(), web3.toWei(articlePrice1, "ether"), "article price must be " + web3.toWei(articlePrice1, "ether"));

        sellerBalanceAfterBuy = web3.fromWei(web3.eth.getBalance(seller), "ether").toNumber();
        buyerBalanceAfterBuy = web3.fromWei(web3.eth.getBalance(buyer), "ether").toNumber();
        assert(sellerBalanceAfterBuy == (sellerBalanceBeforeBuy + articlePrice1), "seller should have received his money:: " + articlePrice1 + " ETH")
        //var gasUsedEtherByBuyer = web3.fromWei(receipt.gasUsed,"ether").toNumber();
        //console.log(receipt.cumulativeGasUsed);
        assert(buyerBalanceAfterBuy <= (buyerBalanceBeforeBuy - articlePrice1), "buyer should have paid gas and article price:: " + articlePrice1 + " ETH");
        return chainListInstance.getArticlesForSale();

      }).then(function (data) {
        assert.equal(data.length, 1, "only 1 item should be sold");
        assert.equal(data[0].toNumber(), 2, "the id of unsold article should be 2");
        return chainListInstance.getNumberOfArticles();
      }).then(function (data){
        assert.equal(data,2,);
      });
    });
  });
});
