var ChainList = artifacts.require("./ChainList.sol");

contract(ChainList, function () {
    var chainListInstance;
    var seller = web3.eth.accounts[1];
    var buyer = web3.eth.accounts[2];
    var articleName = "article 1";
    var articleDescription = "Description for Article 1";
    var articlePrice = 10;

    // no article for sale yet

    it("1. NEGATIVE:: it should throw an exception if you try to buy an article when there is no article", function () {
        return ChainList.deployed().then(function (instance) {
            chainListInstance = instance;
            return chainListInstance.buyArticle(1, {
                from: buyer,
                value: web3.toWei(articlePrice, "ether")
            });
        }).then(assert.fail)
            .catch(function (error) {
                console.log("in catch method");
                assert(true);
            }).then(function () {
                return chainListInstance.getNumberOfArticles();
            }).then(function (data) {
                assert.equal(data.toNumber(), 0, "number of articles must be 0");
            });
    })


    it("2. NEGATIVE:: it should throw an exception if you try to buy an article that does not exist", function() {
        return ChainList.deployed().then(function (instance) {
            chainListInstance = instance;
            return chainListInstance.sellArticle(articleName,
              articleDescription,
              web3.toWei(articlePrice, "ether"),
              { from: seller }
            ); // sell an article
        }).then(function(receipt) {
            return chainListInstance.buyArticle(2, {
                from: buyer,
                value: web3.toWei(articlePrice, "ether")
            }); // try to buy an article with id 2 that does not exist
        }).then(assert.fail)
            .catch(function (error) {
                console.log("in catch method");
                assert(true);
            }).then(function () {
                return chainListInstance.articles(1);
            }).then(function (data) {
                assert.equal(data[0].toNumber(), 1, "id of the article must be 1");
                assert.equal(data[1], seller, "seller must be " + seller);                                
                assert.equal(data[2], 0x0, "buyer must be empty");
                assert.equal(data[3], articleName, "article name must be " + articleName );
                assert.equal(data[4], articleDescription, "article description must be " + articleDescription);
                assert.equal(data[5].toNumber(), web3.toWei(articlePrice, "ether"), "article price must be " + articlePrice);
            });
    })


    it("3. NEGATIVE:: should throw an exception if a seller tries to buy his own articles", function () {
        return ChainList.deployed().then(function (instance) {
            chainListInstance = instance;
            return chainListInstance.buyArticle(1,{
                from: seller,
                value: web3.toWei(articlePrice, "ether")
            });
        }).then(assert.fail)
            .catch(function (error) {
                console.log("*******ERROR******** " + error);
                assert(true);
            }).then(function () {
                return chainListInstance.getNumberOfArticles();
            }).then(function(data){
                assert.equal(data.toNumber(),1,"number of articles for sale must be 1")
                return chainListInstance.articles(1);
            }).then(function (data) {
                assert.equal(data[0].toNumber(), 1, "id of the articles for sale must be 1");
                assert.equal(data[1], seller, "seller must be " + seller);                                
                assert.equal(data[2], 0x0, "buyer must be empty");
                assert.equal(data[3], articleName, "article name must be " + articleName );
                assert.equal(data[4], articleDescription, "article description must be " + articleDescription);
                assert.equal(data[5].toNumber(), web3.toWei(articlePrice, "ether"), "article price must be " + articlePrice);
              //  done();
            }) // end of then
    })


    it("4. NEGATIVE:: should throw an exception if a buyer tries to buy an article for a different price", function () {
        return ChainList.deployed().then(function (instance) {
            return chainListInstance.buyArticle(1, {
                from: buyer,
                value: web3.toWei(articlePrice + 1, "ether")
            });
        }).then(assert.fail)
            .catch(function (error) {
                console.log("*******ERROR******** " + error);
                assert(true);
            }).then(function () {
                return chainListInstance.getNumberOfArticles();
            }).then(function(data){
                assert.equal(data.toNumber(),1,"number of articles for sale must be 1")
                return chainListInstance.articles(1);
            }).then(function (data) {
                assert.equal(data[0].toNumber(), 1, "id of the articles for sale must be 1");
                assert.equal(data[1], seller, "seller must be " + seller);                                
                assert.equal(data[2], 0x0, "buyer must be empty");
                assert.equal(data[3], articleName, "article name must be " + articleName );
                assert.equal(data[4], articleDescription, "article description must be " + articleDescription);
                assert.equal(data[5].toNumber(), web3.toWei(articlePrice, "ether"), "article price must be " + articlePrice);
                //done();
            }) // end of then
    })
    it("5. NEGATIVE:: should throw an exception if a buyer tries to buy an already sold article", function () {
        return ChainList.deployed().then(function (instance) {
            chainListInstance = instance;
            return chainListInstance.buyArticle(1, {
                from: buyer,
                value: web3.toWei(articlePrice, "ether")
            });
        }).then(function () {
            return chainListInstance.buyArticle(1, {
                from: web3.eth.accounts[0],
                value: web3.toWei(articlePrice, "ether")
            });
        }).then(assert.fail)
            .catch(function (error) {
                console.log("*******ERROR******** " + error);
                assert(true);
            }).then(function () {
                return chainListInstance.articles(1);
            }).then(function (data) {
                assert.equal(data[0].toNumber(), 1, "id of the articles for sale must be 1");
                assert.equal(data[1], seller, "seller must be " + seller);                                
                assert.equal(data[2], buyer, "buyer must be " + buyer);
                assert.equal(data[3], articleName, "article name must be " + articleName );
                assert.equal(data[4], articleDescription, "article description must be " + articleDescription);
                assert.equal(data[5].toNumber(), web3.toWei(articlePrice, "ether"), "article price must be " + articlePrice);
                //done();
            }) // end of then
    })
});