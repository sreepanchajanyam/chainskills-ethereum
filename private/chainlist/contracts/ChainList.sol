pragma solidity ^0.4.18;

contract ChainList {
  

  struct Article {
    uint id;
    address seller;
    address buyer;
    string name;
    string description;
    uint256 price;
  }
  

  // state variables
  mapping (uint => Article) public articles;
  uint articleCounter;
  
  // events
  event LogSellArticle(
    uint indexed _id,
    address indexed _seller,
    string _name,
    uint256 _price
  );

  // sell an article
  function sellArticle(string _name, string _description, uint256 _price) public {
    ++articleCounter;
    articles[articleCounter] = Article(
      articleCounter,
      msg.sender,
      0x0,
      _name,
      _description,
      _price
    );
    LogSellArticle(articleCounter, msg.sender, _name, _price);
  }

  event LogBuyArticle(
    uint indexed _id,
    address indexed _seller,
    address indexed _buyer,
    string _name,
    uint256 _price
  );

  // get an article
  // function getArticle(uint _id) public view returns (
  //   address _seller,
  //   address _buyer,
  //   string _name,
  //   string _description,
  //   uint256 _price
  // ) {
  //     Article storage article = articles[_id];
  //     return(article.seller, article.buyer, article.name, article.description, article.price);
  // }

  // function getNumberOfArticles() public view returns(uint) {
  //   return articleCounter;
  // }

  //return all articles for sale
  function getArticlesForSale() public view returns(uint[]) {
    uint[] memory articleIds = new uint[](articleCounter);
    uint numberOfArticlesForSale = 0;
    for (uint i = 1; i <= articleCounter; i++) {
      if (articles[i].buyer == 0x0) {
        articleIds[numberOfArticlesForSale] = articles[i].id;
        numberOfArticlesForSale++;
      }
    
    }
    uint[] memory forSale = new uint[](numberOfArticlesForSale);
    for (uint j = 0; j < numberOfArticlesForSale; j++) {
      forSale[j] = articleIds[j];
    }
    return forSale;
  }

  //return number of articles for sale
  function getNumberOfArticles() public view returns (uint) {

    return articleCounter;
  }

  function buyArticle(uint _id) payable public {
    
    //ensure that there are articles available for sale
    require(articleCounter > 0);

    // ensure that article id is valid
    require(_id > 0 && _id <= articleCounter);
    // retrieve the article from the mapping
    Article storage article = articles[_id];
    // ensure that already is not sold
    require(article.buyer == 0x0);
    // ensure that the seller is not buying the article himself
    require(msg.sender != article.seller);
    // ensure that the value transferred is equal to the price
    require(msg.value == article.price);
    //set the buyer
    article.buyer = msg.sender;
    // transfer money from buyer to seller
    article.seller.transfer(msg.value);
    LogBuyArticle(_id, article.seller, article.buyer, article.name, article.price);
  }
}
