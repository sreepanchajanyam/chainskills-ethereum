App = {
  web3Provider: null,
  contracts: {},
  account: 0x0,
  init: function () {
    // load articlesRow
   // var articlesRow = $('#articlesRow');
   // var articleTemplate = $('#articleTemplate');

    // articleTemplate.find('.panel-title').text('article 1');
    // articleTemplate.find('.article-description').text('Desription for article 1');
    // articleTemplate.find('.article-price').text("10.23");
    // articleTemplate.find('.article-seller').text("0x1234567890123456890");

    // articlesRow.append(articleTemplate.html());

    return App.initWeb3();
  },

  initWeb3: function () {
    /*
    * Replace me...
    */

    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
    } else {

      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }

    web3 = new Web3(App.web3Provider);
    App.displayAccountInfo();
    return App.initContract();
  },

  displayAccountInfo: function(){
    web3.eth.getCoinbase(function(err, account){
      if(!err){
        App.account = account;
        console.log("coin base account::" + account)
        $('#account').text(account);
        web3.eth.getBalance(account, function(err, balance){
          if(!err){
            $('#accountBalance').text(web3.fromWei(balance,"ether")+ " ETH");
          }
        })
      }
    })  
  },

  initContract: function () {
   $.getJSON('ChainList.json', function(chainListArtifact){
      App.contracts.ChainList = TruffleContract(chainListArtifact);
      // set the provider for the contract
      App.contracts.ChainList.setProvider(App.web3Provider);

      //listen to events
      App.listenToEvents();
      // retrieve the article from contract
      return App.reloadArticles();
   });
  },

  reloadArticles: function(){
    //refresh account info
    console.log("in reload articles")
    App.displayAccountInfo();
    $('#artcilesRow').empty();
    App.contracts.ChainList.deployed().then(function(instance){
       return instance.getArticle();
    }).then(function(article){
      console.log("article " + article);
      if(!article[2]){
        console.log("returning...");
        return;
      }
      //retrieve the article template
      var articleTemplate = $('#articleTemplate');
      var articlesRow = $('#articlesRow');
      var price = web3.fromWei(article[4],"ether");
      articleTemplate.find('.panel-title').text(article[2]);
      articleTemplate.find('.article-description').text(article[3]);
      articleTemplate.find('.article-price').text(price);
      articleTemplate.find('.btn-buy').attr('data-value', price);

      var seller = article[0];
      var buyer = article[1];
      if(seller == App.account){
        seller = "You";
      }
      articleTemplate.find('.article-seller').text(seller);
      if(buyer==0x0)  buyer="No One Yet";
      else if(buyer==App.account) buyer= "You";
      
      articleTemplate.find('.article-buyer').text(buyer);
      
      if(article[0] == App.account || article[1] != 0x0){
        articleTemplate.find(".btn-buy").hide();
      } else {
        articleTemplate.find(".btn-buy").show();
      }
      
      console.log(articlesRow)
      articlesRow.append(articleTemplate.html());
      
    }).catch(function(err){
      console.error(err);
    });
    

  },
  sellArticle: function() {
    var _article_name = $('#article_name').val();
    var _description = $('#article_description').val();
    var _price = web3.toWei(parseFloat($('#article_price').val() || 0),"ether");

    if((_article_name.trim()=='') || (_price == 0)){
      return false;
    }
    console.log("selling..." +_article_name +" "+ _description + " " + _price );
    App.contracts.ChainList.deployed().then(function(instance){
      return instance.sellArticle(_article_name, _description, _price, {
        from: App.account,
        gas: 500000
      });
    }).then(function(result){
      //App.reloadArticles();
      console.log("selling success");
    }).catch(function(err) {
      console.error(err);
    })
  },

  listenToEvents: function(){
    console.log("Started Listening for Sell Events");
    App.contracts.ChainList.deployed().then(function(instance){
      instance.LogSellArticle({},{}).watch(function(err, event) {
        console.log("watch event triggered");
        if(!err){
          console.log("no error");
          $("#events").append('<li class="list-group-item">' + event.args._name +' is now for sale </li>');
        } else {
          console.error("some error" + err);
        }
        App.reloadArticles();
      });

      instance.LogBuyArticle({},{}).watch(function(err, event) {
        console.log("watch event triggered");
        if(!err){
          console.log("no error");
          $("#events").append('<li class="list-group-item">' +event.args._buyer + ' bought ' + event.args._name +' </li>');
        } else {
          console.error("some error" + err);
        }
        App.reloadArticles();
      });
    });
  },
  //retrieve the article price

  buyArticle: function() {
    event.preventDefault();

    var _price = parseFloat($(event.target).data('value'));
    App.contracts.ChainList.deployed().then(function(instance) {
      return instance.buyArticle({
        from: App.account,
        value: web3.toWei(_price,"ether"),
        gas: 500000
      });
    }).catch(function(error){
      console.error(error);
    });
  }


};

$(function () {
  $(window).load(function () {
    App.init();
  });
});
