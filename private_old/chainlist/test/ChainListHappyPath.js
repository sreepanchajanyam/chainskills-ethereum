var ChainList = artifacts.require("./ChainList.sol");


contract('ChainList', function(accounts){

    var seller = accounts[1];
    var articleName = "article 1";
    var articleDescription = "Description for article 1";
    var articlePrice = 10;
 

    it("should be initialized with empty values", function(){
        return ChainList.deployed().then(function(instance){
            return instance.getArticle();
        }).then(function(article){
            //console.log(accounts);
            assert.equal(article[0],0x0,"seller must be empty");
            assert.equal(article[1],"","article name must be empty");
            assert.equal(article[2],"","article description must be empty");
            assert.equal(article[3].toNumber(),0,"article price must be zero");
        })
    })

    it("should sell an article", function(){
        return ChainList.deployed().then(function(instance){
            chainListInstance =  instance;
            return chainListInstance.sellArticle(articleName,articleDescription, web3.toWei(articlePrice,"ether"), {from: seller})
        }).then(function() {
            return chainListInstance.getArticle();
        }).then(function(article){
            //console.log(accounts);
            assert.equal(article[0],seller,"seller must be " + seller);
            assert.equal(article[1],articleName,"article name must be "+ articleName);
            assert.equal(article[2],articleDescription, "article description must be " + articleDescription);
            assert.equal(article[3].toNumber(),web3.toWei(articlePrice,"ether"),"article price must be "+ articlePrice);
        })
    })
}
)