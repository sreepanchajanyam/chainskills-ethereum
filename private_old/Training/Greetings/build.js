Web3 = require('web3');
solc = require('solc');
web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545")); // connect to the local testrpc node
sourceCode = fs.readFileSync('Greetings.sol').toString();
compiledCode = solc.compile(sourceCode);
contractABI = JSON.parse(compiledCode.contracts[':Greetings'].interface);
bytecode = compiledCode.contracts[':Greetings'].bytecode;
greetingsContract = web3.eth.contract(contractABI);
greetingsDeployed = greetingsContract.new({data: bytecode, from : web3.eth.accounts[0], gas: 4700000}); // this triggers a txn on blockchain
console.log("address of the contract deployed" + greetingsDeployed.address);
greetingsInstance = greetingsContract.at(greetingsDeployed.address);
console.log("Get Greetings:" + greetingsInstance.getGreetings()); // this does not trrigger a txn on blockchain as this function is only get and defined as constant.
greetingsInstance.setGreetings("Hello ChainSkills", {from : web3.eth.accounts[0]}); // this triggers a transaction on block chain
console.log("Get Greetings After Set:" + greetingsInstance.getGreetings());


